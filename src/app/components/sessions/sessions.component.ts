import { Component, OnInit } from '@angular/core';
import { SessionsService } from '../../services/sessions.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';



@Component({
  selector: 'app-sessions',
  templateUrl: './sessions.component.html',
  styleUrls: ['./sessions.component.css']
})
export class SessionsComponent implements OnInit {
  loading = true;
  sessions: Array<any>;
  description: string;
  form: FormGroup;
  formUpdate: FormGroup;
  closeResult = '';
  isOpen = false;
  updateSessId = '';
  constructor(private sessionsService: SessionsService,
              private formBuilder: FormBuilder,
              private modalService: NgbModal,
              private router: Router) {

    this.form = this.formBuilder.group({
      description: ['', Validators.required]
  });
    this.formUpdate = this.formBuilder.group({
      description: ['', Validators.required]
    });
  }


  ngOnInit(): void {
  const user: any = JSON.parse(localStorage.getItem('user'));
  if (!user || !user.token) {
    this.router.navigate(['/login']);
  }
  this.sessionsService.getSession()
  .subscribe((resp: any ) => {
    this.sessions = resp;
    this.loading = false;
    });
  }
  get f(): any { return this.form.controls; }

  deleteSession(i, id): void {
    this.loading = true;
    this.sessionsService.deleteSession(id)
    .subscribe((resp: any ) => {
      this.sessions.splice(i, 1);
      this.loading = false;
      });
  }

  createSession(): void {
    this.loading = true;
    const description = this.f.description.value;
    this.sessionsService.createSession(description)
    .subscribe((resp: any ) => {
      this.sessions.push(resp);
      this.loading = false;
      });

  }

  updateSession(description): void {
    console.log('update');
    this.sessionsService.modifySession(this.updateSessId, description)
    .subscribe((resp: any ) => {
      for (const ses of this.sessions) {
        if (this.updateSessId === ses.id) {
          ses.description = resp.description;
        }
      }
      this.loading = false;
      });
  }

  open(content, id): void {
    this.updateSessId = id;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      if (result === 'Save click') {
        this.updateSession(this.formUpdate.controls.description.value);
      }
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  logout(): void {
    localStorage.setItem('user', undefined);
    this.router.navigate(['/login']);
  }

}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class SessionsService {

  constructor(private http: HttpClient) { }

  getSession(): any {
    const user: any = JSON.parse(localStorage.getItem('user'));
    const headers =  {
      headers: {
        Authorization: `Bearer ${user.token}`
      }
    };
    return this.http.get(`${environment.apiUrl}/session`, headers);

    }

  createSession( description): any {
    const user: any = JSON.parse(localStorage.getItem('user'));
    const headers =  {
      headers: {
        Authorization: `Bearer ${user.token}`
      }
    };
    const date = new Date();
    return this.http.post(`${environment.apiUrl}/session`, { description, date }, headers);
    }

  modifySession(id, description): any {
    const user: any = JSON.parse(localStorage.getItem('user'));
    const headers =  {
      headers: {
        Authorization: `Bearer ${user.token}`
      }
    };
    return this.http.put(`${environment.apiUrl}/session/${id}`, { id, description }, headers);
    }

  deleteSession(id): any {
    const user: any = JSON.parse(localStorage.getItem('user'));
    const headers =  {
      headers: {
        Authorization: `Bearer ${user.token}`
      }
    };
    return this.http.delete(`${environment.apiUrl}/session/${id}`, headers);
    }
}

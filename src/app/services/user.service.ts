import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient, private router: Router) { }

  login(email, password): any {
    return this.http.post(`${environment.apiUrl}/user/login`, { email, password })
    .subscribe((user: any ) => {
      if (user && user.userid && user.token) {
        localStorage.setItem('user', JSON.stringify(user) );
        this.router.navigate(['/sessions']);
      }
      return user;
      });
    }

  createUser(username, email, password): any {
    return this.http.post(`${environment.apiUrl}/user`, { username, email, password })
    .subscribe((user: any ) => {
      if (user && user.userid && user.token) {
       // this.router.navigate(['/sessions']);
      }
      return user;
      });
    }
  }
